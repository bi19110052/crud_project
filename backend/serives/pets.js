const PetsModel = require('../model/pets');
const mongoose = require('mongoose');
//const pets = require('../model/pets');

module.exports = class PetsService{
    async CreatePets(PetsToCreate){
        const pets = new PetsModel(PetsToCreate);
        return await pets.save();
    }

    async GetAllPets(){
        return await PetsModel.find({});
      
    
    }

    async GetPetsById(id){
        return await PetsModel.findById(id);
       // console.log(typeof(id));
       // const id = req.query.id;
        /*const result= await PetsModel.aggregate([
            {
                $match : {_id:mongoose.Types.ObjectId(id)}
            },
            {
                $lookup: {
                    from:'peoples',
                    localField: 'owner',
                    foreignField:'_id',
                    as: 'people'
                },
            },
            {
                $project: {
                    _id:1,
                    name:1,
                    gender:1,
        
                }
            }
        
       
        ]);
        return result;*/

    }

    async GetPetsDelete(id){
        return await PetsModel.findByIdAndDelete(id);
    }

    async GetPetsUpdate(id, updatedbook){
        return await PetsModel.findByIdAndUpdate(id, updatedbook, {new:true});
    }

    
}



